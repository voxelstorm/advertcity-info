# AdvertCity #
*Broke? Looking to work from THE SKY? Join us in AdvertCity! A huge, procedurally generated place, just waiting to be sold all kinds of junk by YOU!*

AdvertCity is a cyberpunk advertising tycoon game. Explore a massive procedural city, and plaster your adverts all over it. Float around a retro vision of cyberspace and post links online. Watch the economy of the city evolve with the effects of your decisions.

## Info ##
* https://en.wikipedia.org/wiki/AdvertCity
* http://advertcity.wikia.com/wiki/AdvertCity_Wikia

## Store pages ##
* http://voxelstorm.itch.io/advertcity
* http://store.steampowered.com/app/365800/

## Bug reporting ##
* http://code.voxelstorm.com/advertcity/issues